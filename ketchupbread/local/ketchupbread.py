from app import app, db
from app.models import User, GroceryItem

@app.shell_context_processor
def make_shell_context():
    return {"db": db, "User": User, "Grocery_Item": GroceryItem}