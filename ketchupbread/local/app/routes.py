import json
from app import app, db
from os import path, mkdir, urandom
from flask import render_template, request, redirect, url_for, flash
from app.forms import LoginForm
from flask_login import current_user, login_user, logout_user, login_required
from app.models import User, GroceryItem
from werkzeug.urls import url_parse

@app.route('/', methods=['GET'])
def index():
    return redirect(url_for('login'))

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))

@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('grocery_list'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data.lower()).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('grocery_list')
        return redirect(next_page)
    return render_template('login.html', title="Login - Ketchupbread", form=form)


@app.route('/grocery', methods=['GET'])
@login_required
def grocery_list():
    return render_template('grocery.html', title=f"Grocery List - {current_user.username.capitalize()}")

@app.route('/grocery/add_item', methods=['POST'])
def add_item():
    temp_name = request.form['item-entry'].title()

    for i in current_user.grocery_items:
        if i.name == temp_name.strip():
            flash(f"{i.name} is already on the list.")
            return redirect(url_for('grocery_list'))

    g = GroceryItem(name=temp_name.strip(), author=current_user)

    db.session.add(g)
    db.session.commit()

    return redirect(url_for('grocery_list'))

@app.route("/grocery/remove_<string:item_to_remove>", methods=['POST'])
@login_required
def remove_item(item_to_remove):

    for i in current_user.grocery_items:
        if i.name == item_to_remove:
            if i.favorite:
                flash(f"Cannot remove {i.name} as it is a favorite.")
                return redirect(url_for('grocery_list'))
            db.session.delete(i)

    db.session.commit()

    return redirect(url_for('grocery_list'))

@app.route("/grocery/favorite_<string:item_to_favorite>", methods=['POST'])
@login_required
def favorite_item(item_to_favorite):
    for i in current_user.grocery_items:
        if i.name == item_to_favorite:
            i.favorite = True

    db.session.commit()

    return redirect(url_for('grocery_list'))

@app.route("/grocery/unfavorite_<string:item_to_unfavorite>", methods=['POST'])
@login_required
def unfavorite_item(item_to_unfavorite):
    for i in current_user.grocery_items:
        if i.name == item_to_unfavorite:
            i.favorite = False

    db.session.commit()

    return redirect(url_for('grocery_list'))

@app.after_request
def add_header(r):
    r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    r.headers["Pragma"] = "no-cache"
    r.headers["Expires"] = "0"
    r.headers['Cache-Control'] = 'public, max-age=0'
    return r

if __name__ == "__main__":
    app.run(debug=True, port=80)