cd ./ketchupbread
export FLASK_ENV=production
export FLASK_APP=ketchupbread.py
export FLASK_RUN_HOST=0.0.0.0
export FLASK_RUN_PORT=80
bin/activate

python3 -m flask run
